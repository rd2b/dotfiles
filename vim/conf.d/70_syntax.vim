""" Vimrc config file
""" 70_syntax.vim : Syntax tool
""" author=Remi Debay
""" date=2019/04/02

" Use with:
"  source 70_syntax.vim

"" Plugin lint pour vim
call plug#begin('~/.vim/plugged')
Plug 'vim-syntastic/syntastic'
Plug 'lepture/vim-jinja'
Plug 'vim-airline/vim-airline'
Plug 'jceb/vim-orgmode'
call plug#end()

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_python_exec = 'python3'
let g:syntastic_python_checkers = ['python']
